# Architectural Decision Records

[Documenting Architecture Decisions](https://cognitect.com/blog/2011/11/15/documenting-architecture-decisions)

## AWS Aurora

* Initial development with PostgreSQL 12.4.
* PostgreSQL supports indexing and queries on schemaless documents (JSONB) similar to MongoDB.
* Organisation wide familiarity.
* Multi-region autoscaling.
* [Beneficiary Query Service Database](https://docs.google.com/document/d/1zQ0hxLk4IdpUA4S6KyyU8s6CozLNjnN5Q4QZfw78kJw/edit?usp=sharing)

## Confluent's Python Client for Apache Kafka

* Most active community.
* Well supported and good documentation.
* Simple to use and understand.
* [Python Kafka Library Comparison](https://docs.google.com/document/d/1lGKW9NEZ3gkMTVFNknmlPgvH_NWHDqhKoCvkZTYth-0/edit?usp=sharing)

## Apache Avro Serialisation Protocol

* Native standard used throughout the confluent and Kafka ecosystem.
* Good AsyncAPI integration for interface documentation.
* Compact and efficient.
* [Evaluating Serialisation Standards For Kafka Payloads](https://docs.google.com/document/d/1BIN5JPlAanU_bHuqRjxIWrU20G77NRURBwb1Ewko8uA/edit?usp=sharing)

## Domain Topics are Complete and Compacted 

* Domain topics record the entire current state of the domain and are historically complete. 
* New services can create and maintain local replicas of the data from the domain topics alone.
* Services can migrate to new local schemas by re-processing all the events in the domain topic.
* This provides a simple mechanism to create and evolve new services independently.

## Python 3.8.8
 * Common choice for docker images supporting services
 * Most recent release of Python with wide package support
 * As defined by the _proposed_ RFC on _'managing software versions and upgrades'_

## Ubuntu 20.04.2 (Focal Fossa)
 * As defined by the _proposed_ RFC on _'managing software versions and upgrades'_









