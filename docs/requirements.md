# Requirements

## Query Service

### Summary

* Provide a definitive REST API query endpoint to access a clients beneficiaries - as part of 
the *Beneficiary Management Service.* 
  
* Provide a compatibility endpoint which is backward compatible with the *'client/beneficiaries'*  
endpoint provided by BOS to EBO. This will wrap, or substantially reuse the definitive endpoint functionality.