

### Event Driven Query Service

Beneficiaries are created in the BOS database as usual and Debezium/Kafka Connect are used to snapshot
and track changes so that the state can be replicated outside BOS.

CDC events are aggregated and Beneficiary creation, update and delete commands are generated reflecting the activity in
BOS.

The Query Service listens to the Domain events and maintains its own representation of beneficiaries in a format the
business domain requires (read models). The queries that access this data are simple, lightweight and fast.

Every time the reference data changes, the read models are updated. On each change to the reference data, business
logic is invoked  to aggregate and recompute the query models.

Read models are optimised to support the access patterns of the use case. Different read models can be provided for
different use cases.

### Query Service Components

![Query Service Components](images/ebury_2_0_query_service..png)

**1:** Beneficiaries will continue to be created/updated/deleted directly in BOS via existing channels.

**2:** Kafka Connect forms part of the outbound gateway publishing Create, Update and Delete (CUD) events for changes
in BOS. These Change Data Capture (CDC) events are generated for the BOS data required by the Beneficiary
Command Service.

**3 & 4:** A Beneficiary data aggregation service consumes the BOS CDC events and publishes commands to the Beneficiary
Command Service. BOS Beneficiary data is stored across multiple relational tables and the CDC events mirror changes
to these tables. The order in which these events are received is not defined and they must be aggregated to generate
well formed commands for the Beneficiary Command Service.

**5 & 6:** The Beneficiary Command Service consumes and actions commands, and publishes Domain Events on successful
completion. The commands can be any action that is required of the Beneficiary Command Service.

**7:** Domain Events are consumed and aggregated to form a read model. The read model is designed for specific use
cases and optimised for those access patterns.

**8:** A synchronous external facing API is provided to query the read model. The queries are simple and fast as the
data is precomputed to optimise read performance.


