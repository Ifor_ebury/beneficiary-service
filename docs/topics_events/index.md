
## Overview

### Events and Schemas 

Event schemas are defined using AVRO's schema definition language.

The schemas and description for all the events in this service are contained in the `schemas` folder. 

### Partitioning Strategy

### Retention Periods

### Compaction

## Domain Topics 

Complete set of durable domain facts that can be used to rebuild local state.

### `ebury.beneficiaries.beneficiary.events`

| Parameter      | Value       |
| ---------------|-------------|
| partitions     | 1           |
| cleanup.policy | compact     |

## Command Topics 

Commands requesting action from the domain service. 
A high volume activity that needs to be scalable.  

### `ebury.beneficiaries.beneficiary.commands`

| Parameter      | Value       |
| ---------------|-------------|
| partitions     | **128**     |
| cleanup.policy | delete |
| retention.ms | 7 days |

## Notification Topics 

Notifications produced by components when they have completed some action. 

### `ebury.beneficiaries.beneficiary.notifications`

| Parameter      | Value       |
| ---------------|-------------|
| partitions     | 1         |
| cleanup.policy | delete |
| retention.ms | 7 days |

## CDC Topics 

Change Data Capture events generated by Debezium and Kafka Connect from the BOS database. 

### `public.clients_beneficiary`
### `public.core_address`
### `public.clients_beneficiaryemail`
### `public.clients_beneficiarygroup`
### `public.clients_beneficiarybatch`
### `public.clients_client`
### `public.settlements_bankaccount`
### `public.core_country`
### `public.core_emailsent`


| Parameter      | Value       |
| ---------------|-------------|
| partitions     | 1         |
| cleanup.policy | delete |
| retention.ms | 14 days |





