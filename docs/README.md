# README

These are the detailed documents for The Beneficiary Management Service

The aim of these docuemtns are to :-
* clarify requirements
* document design decisions
* document the architectural components
* document topics, events and event flows
* provide an index to other specifications within the project (e.g. schemas)

These are living documents that change along with the code in the repo.

Start [HERE](index.md)