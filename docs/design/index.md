

## Design Documentation
* [CDC Aggregation](cdc_aggregation.md)
* [Command Service](command_service.md)
* [Query Service](query_service.md)
* [Common Event Library](event_library.md)